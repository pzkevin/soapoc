<?php

namespace Database\Factories;
use Illuminate\Support\Str;


use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class ClienteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Hash::make('123123123'), // password
            'celular'=> $this->faker->phoneNumber(),
            'domicilio'=> 'Ave Divisaderos #244',
            'rfc'=> 'PESK001007UM2',
        ];
    }
}
