<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('clientes')->insert([
            'nombre'=> Str::random(10),
            'email'=> Str::random(10).'@gmail.com',
            'celular'=> Str::random(10),
            'domicilio'=> 'Ave Divisaderos #244',
            'rfc'=> 'PESK001007UM2',
            'pasword'=> Hash::make('123123123')
        ]);
    }
}
