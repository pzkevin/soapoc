<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    
    //Los campos deben estar como FILLABLES
    protected $fillable = [
            'nombre',
            'email',
            'celular',
            'domicilio',
            'rfc',
            'password'
    ];
}
