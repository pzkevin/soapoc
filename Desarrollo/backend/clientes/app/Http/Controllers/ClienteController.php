<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Http\Requests\StoreClienteRequest;
use App\Http\Requests\UpdateClienteRequest;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //SELECT - GET - Devuelve todo los clientes de la BD
        /**
         * Definir la conexión
         * Conexión
         * Hacer el query
         * Validar el query
         * Retornar resultado
         */
        $clientes = Cliente::all();
        //Si no hay clientes
        //if(!clientes)
        //    return response()->json(["MSG"=>"NO HAY CLIENTES REGISTRADOS AÚN"], 404);
        //Devolvemos un JSON
        return response()->json($clientes, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClienteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClienteRequest $request)
    {
        //INSERT - POST - almacena un nuevo cliente en la BD
        //guardar nuevo REGISTRO en la tabla que este asociada al MODELO
        //Cliente
        $cliente = Cliente::create([
            'nombre'=>$request->nombre,
            'email'=>$request->email,
            'celular'=>$request->celular,
            'domicilio'=>$request->domicilio,
            'rfc'=>$request->rfc
        ]);
        //Si no se crea o es null
        if (!$cliente)
            return response()->json("ERROR al insertar", 400);
        if ($cliente)
            return response()->json('Cliente <'.$request->email . '> Creado con éxito.', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //SELECT - GET Devuelve un solo cliente con un ID
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClienteRequest  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClienteRequest $request, Cliente $cliente)
    {
        //UPDATE - PUT - Actualiza los datos del cliente con el ID de la BD
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //DELETE - DELETE - Borra el RECURSO
    }
}
