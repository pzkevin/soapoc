-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-12-2021 a las 23:08:59
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `soa_clientes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domicilio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfc` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `email`, `celular`, `domicilio`, `rfc`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Cielo Oberbrunner', 'ibergstrom@example.com', '1-518-431-7403', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$G/MzozLhBQ3F9p2ve7xe7eOB2JdsaVNfJK4SYv2EHu8yPSIBkpshO', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(2, 'Trent Schaden DVM', 'alda.wuckert@example.com', '(203) 469-1564', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$caDvVI2yM0sEfdmbXPxMpOFCKVBaNAl3qDWDe/N6GcynnRQ7keqJm', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(3, 'Mr. Omari Jast III', 'colt.hermann@example.net', '920.597.7739', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$67cPNAGrmP5VsUtOUUV2e.GCTtgAAONbusTGslR3mKR7U3UF0cDmm', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(4, 'Mr. Victor Keeling V', 'ipagac@example.com', '615.895.3214', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$ez.Uf454A/KU21TJGI/nreJ5UhtDHQWitOkCvTBhVJEjatKF0ZIe6', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(5, 'Clinton Homenick', 'coy67@example.net', '(364) 354-5111', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$wlk9HOGEsIsYmJlbIlKCdeQ5cE95by5LjhMBLjjopAQwKSvpT/pG.', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(6, 'Ms. Zora Halvorson I', 'purdy.waylon@example.com', '+1-407-513-5670', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$a.GLx83U4cAYlbNLXQyz2OfsYmAhb3JOZHlOr4S5I8dYAmlG/btFO', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(7, 'Prof. Emmalee Reynolds', 'lang.rupert@example.org', '747-255-7606', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$GKcw.DdgISZpy7I6aQKyEuc0XRwqMHFqrrOEwQEfQXEveJte5L4D6', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(8, 'Alanna Dickinson DVM', 'enid00@example.net', '+1-320-774-2838', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$GN5Mjaq0dT2LPcuS3T2yG.WXWfyMFgWcZyCVmmYTB82MRhhbwM6X.', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(9, 'Alysha McGlynn', 'hill.dameon@example.net', '(225) 383-7219', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$nOjfjmijUKMGqmaANRwNIOcISLGkMzHCYAELkF49pzGcntf1PHJjW', '2021-12-07 05:06:25', '2021-12-07 05:06:25'),
(10, 'Dr. Gavin Hauck IV', 'brielle.predovic@example.com', '(934) 880-9620', 'Ave Divisaderos #244', 'PESK001007UM2', '$2y$10$APa6gWDko.Q3xaHx/1LOAu2h8cG1fFsOQqzGMW3R3VSfc9pdXRF0y', '2021-12-07 05:06:25', '2021-12-07 05:06:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_12_06_204727_create_clientes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clientes_email_unique` (`email`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
